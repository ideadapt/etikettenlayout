# Preconditions
1. You as developer have a Google account
2. Any user using this app has a Google account


# Setup

## Google Cloud Project
1. Login to https://console.cloud.google.com/
2. Create a project and select it as active project

## OAuth
1. Go to API credentials page https://console.cloud.google.com/apis/credentials
2. Create an OAuth 2.0 Client ID. OAuth is required, so that the user can give this app the authorization to access his resources stored on Google (such as Google Drive). 
3. Add some JavaScript origins. Each has to exactly match the origin (protocol + host + port) your app is hosted on, e.g. http://localhost:5500 . This is required to make the OAuth flow work.
4. Add some redirect URLs, e.g. http://localhost:5500/src/ . This is required to make the OAuth flow work.
5. Copy the Client ID to your config.mjs .
6. Add test users in consent screen configuration https://console.cloud.google.com/apis/credentials/consent

## Google Cloud Spreadsheet
TBD: template

### AppScript
TBD: services, code, deployments, config
1. Create an Apps Script for the Spreadsheet by using the menu command "Extensions > Apps Script".
2. Use the code from appscript.js (Code.gs) and appscript.json (create it manually). Set your folder ID in appscript.js.
3. Add "Drive" as Service.
4. Deploy it as "API Executable". Copy the API Executable URL to your config.mjs.

## PDF Generation
This repository uses the API of https://html2pdf.fly.dev/ by default. You can also host it by yourself.


# Development
Start a static web server locally to serve this directory:

`python3 -m http.server 5500`

`open http://localhost:5500/src` in a Chrome browser.

## Debugging
To also debug PDF generation, run https://github.com/ideadapt/puppeteer-html-to-pdf-converter on localhost:3000.
Generate PDF from any HTML file. Avoid using localhost URLs in input data (url, image urls), because they underlay some security restrictions.
To get a localhost alias (e.g. local.host) add it to your /etc/hosts file.

```
python3 -m http.server 5500
# printer has 2mm margin
curl -X POST --data '{"url": "http://local.host:5500/border3x6.html", "format": "A4", "margin.bottom": "0mm", "margin.top": "0mm", "margin.left": "0mm", "margin.right": "0mm"}' -H 'content-type:application/json' http://localhost:3000/generate
``` 


# Deploy
Upload entire src folder: 

```
(cd src && scp -r . cyon:~/www/setzlinge/)
```

The deployed folder has to be accessible via one of the JavaScript origins in the OAuth configuration.


# Customize
In `src/index.mjs` the functions `generateModelsFromSheet` and `generateHTML` contain all logic required to create a custom print layout.
The two default layouts contained in this repository are 3x6 and 4x2.


# What else could be done?
- Image crop editor
- CSS cache invalidation almost never happens, which is terribly bad UX
