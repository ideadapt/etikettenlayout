import htm from './libs/htm.mjs'
import vhtml from './libs/vhtml.mjs'
import config from './config.mjs'
import layout2x4 from './layout-4x2-A4-land.css' assert {type: 'css'}
import layout3x6 from './layout-3x6-A4-port.css' assert {type: 'css'}
import layoutBase from './layout-base.css' assert {type: 'css'}
import {debounce, generateId, sequence, styleToString} from "./libs/utils.mjs"
import {hasToken, isTokenValid, logout, parseHashParams, signInWithOAuth, storeTokenValidity} from './libs/oauth.mjs'

const htmlt = htm.bind(vhtml)

const css2x4 = styleToString(layout2x4);
const css3x6 = styleToString(layout3x6);
const cssBase = styleToString(layoutBase);

(async function () {
  const apiKey = config.apiKey
  const spreadsheetId = config.spreadsheetId
  const folderId = config.folderId
  const updates = new Map()

  async function getModelsFromSheet(spreadsheetId, apiKey) {
    const data = await fetch(`https://sheets.googleapis.com/v4/spreadsheets/${spreadsheetId}/values/list!A:I?key=${apiKey}`, {
      headers: {
        "Content-Type": "application/json"
      }
    })

    if (data.ok) {
      const body = await data.json()
      /**
       [
       [ 'Print', 'Titel', 'ProSpecieRara', 'Bild', 'Thumbnail', 'Text', 'ThumbURL', 'Hotness' ],
       [
       'Sonnenblume verzweigt',
       'TRUE',
       'sonnenblume-verzweigt.jpg',
       'Ideal als Schnittblume und positiv für bestäubende Insekten. Die Wuchsform ist verzweigt und bis 2m hoch. Die Blüten blühen in verschiedenen Gelb- und Brauntönen.',
       'https://drive.google.com/thumbnail?id=1NJzUi4dtqGUICfmCoLMSxUku4-7mcfhY&sz=h100',
       '9+'
       ],
       */
      const [cols, ...cardData] = body.values
      const printableCardData = cardData
        .map((rowData, i) => ({ ...rowData, sheet: { rowNumber: i + 2 } }))
        .filter(rowData => rowData[0] === 'TRUE')
      const cardModels = printableCardData.map(rowData => {
        const row = Object.create(null)
        row.sheet = rowData.sheet
        cols.forEach((col, i) => {
          row[col] = rowData[i]
        })
        return row
      }).map(row => {
        const hotness = row['Hotness']
        let hot_word = null
        if (hotness != null && hotness.length > 0) {
          const hot_number = Number(hotness.substring(0, 2))
          const cat_index = [[0, 1], [2, 3, 4], [5, 6, 7], [8], [9, 10]].findIndex(r => r.includes(hot_number))
          hot_word = ['mild', 'pikant', 'scharf', 'sehr scharf!!', 'sehr scharf!!!'][cat_index]
        }

        return Object.assign(Object.create(null), {
          h1: row.Titel,
          filename: row.Titel.replaceAll(/\s+/g, '-').replaceAll(/[^a-z.-]/gi, '').toLowerCase(),
          id: generateId(10),
          teaser: row.ThumbURL,
          text: row.Text,
          header: row.ProSpecieRara === 'TRUE' ? '<img src="https://drive.google.com/thumbnail?id=16XBfZziq1_PbkFUcwomybtOwAn2ivAUF" class="icon psr"  alt="ProSpecieRara"/>' : '',
          hot_css_class: hot_word === null ? 'hidden' : '',
          hot_word,
          hot_value: hotness,
          layout: row['Layout'],
          sheet: {
            ...row.sheet,
            ranges: {
              Titel: `B${row.sheet.rowNumber}:B${row.sheet.rowNumber}`,
              Text: `F${row.sheet.rowNumber}:F${row.sheet.rowNumber}`
            }
          }
        })
      })
      return cardModels
    } else {
      throw new Error(await data.json())
    }
  }

  function generateHTML(model, layout) {
    return htmlt`<root>
    <html lang="de">
    <head>
        <meta charset="UTF-8">
        <title>${model.h1}</title>
    </head>
    <body class="${layout.orientation}">
    <style>${cssBase + layout.css}</style>
    <div class="page row${layout.rows} col${layout.cols} ${layout.orientation} ${layout.preview ? 'editor' : ''}">
    ${sequence(layout.preview ? 1 : layout.cols * layout.rows).map(_ => htmlt`
      <div class="etikett">
        <article>
          <header>
            <img src="${config.logoUrl}" class="logo"  alt="Logo"/>
            ${model.header}
          </header>
          <h1 contenteditable="${layout.preview && window.accessToken ? 'true' : 'false'}" data-col='Titel'>${model.h1}</h1>
          <div class="teaser">
              <img src="${model.teaser}"  alt="Teaser"/>
          </div>
          <p class="${model.hot_css_class}"><em>Schärfeskala: ${model.hot_value} von 10+</em><span>${model.hot_word}</span></p>
          <p contenteditable="${layout.preview && window.accessToken ? 'true' : 'false'}" data-col='Text'>${model.text}</p>
          <footer>Dolma Kunz, Aktienstrasse 7, 7411 Sils i.D.</footer>
        </article>
      </div>
    `)}
    </div>
    <script>
      const ranges = JSON.parse('${JSON.stringify(model.sheet.ranges)}')
      document.addEventListener('input', ({target}) => {
        parent.postMessage(JSON.stringify({range: ranges[target.dataset.col], rowNumber: ${model.sheet.rowNumber}, col: target.dataset.col, value: target.value || target.innerText}), document.location.origin)
      })
    </script>    
    </body>
    </html></root>
    `[1]
  }

  function createPDFDownloadLink(blob, model) {
    const objectURL = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.innerText = model.h1;
    a.href = objectURL;
    a.download = model.h1.replaceAll(/[^\w]/gi, '-');
    return a;
  }

  async function generatePDF(html, model, layout) {
    const res = await fetch(config.PdfGeneratorUrl, {
      method: 'POST',
      headers: {
        'content-type': "application/json",
        accept: 'application/json',
      },
      body: JSON.stringify({
        html,
        format: "A4",
        filename: model.filename,
        landscape: layout.orientation === 'landscape'
      })
    })

    if (res.ok) {
      const blob = await res.blob()
      console.log(res, await blob.text());
      return new Blob([blob], { type: 'application/pdf' })
    } else {
      throw await res.json()
    }
  }

  function createLayout(layoutName) {
    if (layoutName === '3x6') return {css: css3x6, cols: 3, rows: 6, orientation: 'portrait', preview: false}
    if (layoutName === '2x4') return {css: css2x4, cols: 4, rows: 2, orientation: 'landscape', preview: false}
    throw {message: 'unknown layout ' + layoutName}
  }

  function createUpdateMessageListener() {
    const updateSheet = debounce(function (updates) {
      console.log('sending updates to sheet', updates);
      for (const update of updates.values()) {
        fetch(`https://sheets.googleapis.com/v4/spreadsheets/${spreadsheetId}/values/list!${update.range}?valueInputOption=raw`, {
          method: 'PUT',
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + window.accessToken
          },
          body: JSON.stringify({range: 'list!' + update.range, values: [[update.value]]})
        })
      }
    }, 2000)

    return function onMessage(event) {
      if (event.origin !== document.location.origin) {
        return;
      }
      const data = JSON.parse(event.data)
      updates.set(data.rowNumber, data)
      updateSheet(updates)
    }
  }

  const onMessage = createUpdateMessageListener()

  function applyAuthState() {
    [...document.getElementsByClassName('login-hint')].forEach(el => el.addEventListener('click', () => signInWithOAuth()))
    const params = parseHashParams()
    if (params.has('access_token')) {
      window.accessToken = params.get('access_token')
      document.getElementById('preview').classList.remove('disabled')
      document.querySelectorAll('.login-hint').forEach(el => el.remove())
      storeTokenValidity()
    }

    if (hasToken() && !isTokenValid()) {
      logout()
    }
    if (hasToken() && isTokenValid()) {
      console.log('logged in')
    }
  }

  document.addEventListener('DOMContentLoaded', () => {

    if (window.navigator.userAgent.match(/chrome/i)) {
      document.getElementById('browser-compat').remove()
    }

    applyAuthState();

    const fileEl = document.getElementsByName('newFile')[0]
    fileEl.addEventListener('change', () => {
      fileEl.setAttribute('disabled', 'disabled')
      const file = fileEl.files[0]
      const metadata = {
        'name': file.name,
        'mimeType': file.type,
        'parents': [folderId],
      };

      const fileUploadEl = document.getElementsByClassName('file-upload')[0]
      const accessToken = window.accessToken
      const form = new FormData();
      form.append('metadata', new Blob([JSON.stringify(metadata)], { type: 'application/json' }));
      form.append('file', file);
      fileUploadEl.classList.add('uploading')
      fileUploadEl.classList.remove('error')

      fetch('https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart&fields=id', {
        method: 'POST',
        headers: new Headers({ 'Authorization': 'Bearer ' + accessToken }),
        body: form,
      })
          .then(resp => resp.json())
          .then(respBody => {
            if (!respBody.error) {
              // Trigger image sync in spreadsheet by simply calling the script
              // https://developers.google.com/apps-script/api/reference/rest/v1/scripts/run?hl=en
              return fetch(config.appScriptExecutableUrl, {
                method: 'POST',
                body: JSON.stringify({
                  function: 'GdriveFiles',
                  devMode: false
                }),
                headers: new Headers({ 'Authorization': 'Bearer ' + accessToken, 'Content-Type': 'application/json' }),
              })
            } else {
              throw new Error(respBody.error.message)
            }
          })
          .then(() => {
            fileUploadEl.classList.add('done')
            window.setTimeout(() => fileUploadEl.classList.remove('done'), 2000)
          })
          .catch(() => {
            fileUploadEl.classList.add('error')
          })
          .finally(() => {
            fileEl.removeAttribute('disabled')
            fileUploadEl.classList.remove('uploading')
          })
    })

    document.getElementById('preview').addEventListener('click', async () => {
      const cardModels = await getModelsFromSheet(spreadsheetId, apiKey)
      const previewContainer = document.getElementById('preview-container')
      previewContainer.innerHTML = ''

      for (const model of cardModels) {
        const layout = createLayout(model.layout)
        layout.preview = true
        const html = generateHTML(model, layout)

        const frame = document.createElement('iframe')
        previewContainer.appendChild(frame)
        frame.src = 'about:blank'
        frame.contentWindow.document.write(html)

        window.setTimeout(() => {
          frame.setAttribute('height', frame.contentWindow.document.body.scrollHeight + 20 + 'px')
          frame.setAttribute('width', frame.contentWindow.document.body.scrollWidth + 20 + 'px')
        }, 450)
      }

      window.removeEventListener('message', onMessage)
      window.addEventListener('message', onMessage, false);
    });

    document.getElementById('generate').addEventListener('click', async () => {
      const cardModels = await getModelsFromSheet(spreadsheetId, apiKey)
      const traceEl = document.getElementById('trace');
      const tableEl = traceEl.parentElement

      traceEl.innerHTML = ''

      for (const model of cardModels) {
        const rowEl = document.createElement('tr');
        traceEl.appendChild(rowEl)
        tableEl.classList.remove('d-none')

        const tableRow = htmlt`<tr data-id="${model.id}">
<td>${model.h1}</td>
<td><a href="#" class="pdf-link"><i class="bi-filetype-pdf"></i></a></td>
<td><a href="#" class="html-link"><i class="bi-filetype-html"></i></a></td>
</tr>`
        rowEl.outerHTML = tableRow
        const tr = traceEl.querySelector(`[data-id="${model.id}"]`)
        const tdLabel = tr.firstChild

        try {
          const pdfLink = tr.querySelector('.pdf-link')
          const htmlLink = tr.querySelector('.html-link')

          const layout = createLayout(model.layout)
          const html = generateHTML(model, layout)
          const pdfDownloadLink = createPDFDownloadLink(await generatePDF(html, model, layout), model)
          pdfLink.addEventListener('click', (ev) => {
            ev.preventDefault()
            pdfDownloadLink.click();
          })
          htmlLink.addEventListener('click', (ev) => {
            ev.preventDefault();
            const blob = new Blob([html], { type: "text/html" });
            window.open(URL.createObjectURL(blob), "_blank");
            return false;
          })
        } catch (ex) {
          console.error(ex)
          tr.classList.add('table-danger')
          tdLabel.innerHTML = ex.message ? ex.message : ex.errors.map(JSON.stringify).join('<br/>')
        }
      }
    })
  })
})()
