// Copy this file as config.mjs
export default {
    // Get from Google Cloud Console
    apiKey: '...',
    // Extract from spreadsheet URL
    spreadsheetId: '...',
    // Extract from share URL
    // e.g. https://drive.google.com/drive/folders/1zof_oc55oF2iiiiwu8rbIuAVuqY5Uoof?usp=sharing
    folderId: '...',
    PdfGeneratorUrl: 'https://html2pdf.fly.dev/api/generate',
    // "API Executable" URL of the "API Executable" deployment
    appScriptExecutableUrl: 'https://script.googleapis.com/v1/scripts/...:run',
    oAuthClientId: '....apps.googleusercontent.com',
    logoUrl: '...'
}