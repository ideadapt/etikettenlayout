// This file contains the Google Apps Script code of the spreadsheet

/**
 * Add all files of a Google Drive folder as rows to the "images" spreadsheet.
 *
 * @returns {{data: *[], count: number}}
 * @constructor
 */
function GdriveFiles() {
    // TODO set folder ID. Has to equal the folderId in config.mjs
    const folderId = '1zof_oc55oF2iicNwu8rbIuAVuqY5Uoof'
    const folder = DriveApp.getFolderById(folderId)
    const files = folder.getFiles()
    const source = SpreadsheetApp.getActiveSpreadsheet();
    const sheet = source.getSheetByName('images');

    const data = [];
    const startRow=2;
    let row = startRow;
    while (files.hasNext()) {
        const childFile = files.next();
        const id = childFile.getUrl().match('[a-zA-Z0-9_-]{20,}');
        const info = [
            childFile.getName(),
            childFile.getUrl(),
            id,
            'https://drive.google.com/thumbnail?id='+id+'&sz=h300',
            'https://drive.google.com/uc?export=view&id='+id,
            '=IMAGE(D'+row+')',
        ];
        data.push(info);
        ++row;
    }

    sheet.getRange(2, 1, data.length, data[0].length).setValues(data);
    return { count: data.length, data }
}