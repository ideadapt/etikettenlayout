export function styleToString(stylesheet) {
    return stylesheet.cssRules
        ? Array.from(stylesheet.cssRules)
            .map(rule => rule.cssText || '')
            .join('\n')
        : ''
}

export function sequence(nr) {
    return Array.from(Array(nr).keys()).map(colI => colI + 1);
}

// dec2hex :: Integer -> String
// i.e. 0-255 -> '00'-'ff'
function dec2hex(dec) {
    return dec.toString(16).padStart(2, "0")
}

// generateId :: Integer -> String
export function generateId(len = 40) {
    const arr = new Uint8Array((len) / 2)
    window.crypto.getRandomValues(arr)
    return Array.from(arr, dec2hex).join('')
}

export function debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => { func.apply(this, args); }, timeout);
    };
}