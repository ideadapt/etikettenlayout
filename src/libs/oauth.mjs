/*
 * From https://developers.google.com/identity/protocols/oauth2/javascript-implicit-flow?hl=en#oauth-2.0-endpoints_1
 * Create form to request access token from Google's OAuth 2.0 server.
 */

import config from "../config.mjs";

export function signInWithOAuth() {
    const oauth2Endpoint = 'https://accounts.google.com/o/oauth2/v2/auth';

    const form = document.createElement('form');
    form.setAttribute('method', 'GET');
    form.setAttribute('action', oauth2Endpoint);

    const params = {
        'client_id': config.oAuthClientId,
        'redirect_uri': window.location.origin + window.location.pathname,
        'response_type': 'token',
        'scope': 'https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/spreadsheets',
        'include_granted_scopes': 'false'
    };

    for (const p in params) {
        const input = document.createElement('input');
        input.setAttribute('type', 'hidden');
        input.setAttribute('name', p);
        input.setAttribute('value', params[p]);
        form.appendChild(input);
    }

    // Open the OAuth 2.0 endpoint (HTML from IdP)
    document.body.appendChild(form);
    localStorage.removeItem('expires_at')
    form.submit();
}

export function parseHashParams(){
    const s = location.hash || ''
    if(s.startsWith('#') && s.length > 1){
        return new URLSearchParams(s.substring(1))
    }
    return new URLSearchParams()
}

export function storeTokenValidity(){
    const p = parseHashParams()
    if(p.has('expires_at')) return;

    const expiresAt = new Date().getTime() + Number(p.get('expires_in')) * 1000
    p.set('expires_at', expiresAt)
    location.hash = '#' + p.toString()
}

export function hasToken(){
    const expAt = parseHashParams().get('expires_at')
    return expAt != null
}

export function isTokenValid(){
    const expAt = parseHashParams().get('expires_at')
    return expAt != null && (new Date() < new Date(Number(expAt)))
}

export function logout(){
    const p = parseHashParams()
    p.delete('expires_at')
    window.location.hash = ''
    window.location.reload()
} 